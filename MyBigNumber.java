public class MyBigNumber {
    public static String sum(String stn1, String stn2) {
        String result = "";
        int len1 = stn1.length();
        int len2 = stn2.length();
        int maxlen = (len1 > len2) ? len1 : len2;
        int index1, index2, sum, carry = 0;
        for (int i = 0; i < maxlen; i++) {
            index1 = len1 - i - 1;
            index2 = len2 - i - 1;
            int digit1 = (index1 >= 0) ? Character.getNumericValue(stn1.charAt(index1)): 0;
            int digit2 = (index2 >= 0) ? Character.getNumericValue(stn2.charAt(index2)) : 0;
            sum = digit1 + digit2 + carry;
            result = (sum % 10) + result;
            carry = sum / 10;
        }
        if (carry > 0) {
            result = carry + result;
        }
        return result;
    }
}
